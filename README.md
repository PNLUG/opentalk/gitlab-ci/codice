# CI/CD GitLab @ PNLug

A Simple demostration of CI/CD features present in GitLab.

More info about the event [this](https://www.pnlug.it/2018/05/25/serata-a-tema-workshop-su-gitlab/).

Slides are available [here](https://slides.pnlug.it/2018/gitlab-ci).
